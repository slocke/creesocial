<?php

/**
 * This is the model class for table "posts".
 *
 * The followings are the available columns in table 'posts':
 * @property string $uid
 * @property string $description
 * @property string $source_type
 * @property string $source_name
 * @property string $source_url
 * @property string $avatar_url
 * @property string $title
 * @property string $url
 * @property string $published_at
 * @property string $id
 * @property integer $tone
 * @property double $tone_score
 * @property string $alert_id
 */
class Post extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'posts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, tone, tone_score, alert_id', 'required'),
			array('tone', 'numerical', 'integerOnly'=>true),
			array('tone_score', 'numerical'),
			array('source_type, source_name, source_url, avatar_url, title, url, published_at', 'length', 'max'=>255),
			array('id, alert_id', 'length', 'max'=>11),
			array('description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('uid, description, source_type, source_name, source_url, avatar_url, title, url, published_at, id, tone, tone_score, alert_id', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'uid' => 'Uid',
			'description' => 'Description',
			'source_type' => 'Source Type',
			'source_name' => 'Source Name',
			'source_url' => 'Source Url',
			'avatar_url' => 'Avatar Url',
			'title' => 'Title',
			'url' => 'Url',
			'published_at' => 'Published At',
			'id' => 'ID',
			'tone' => 'Tone',
			'tone_score' => 'Tone Score',
			'alert_id' => 'Alert',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('uid',$this->uid,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('source_type',$this->source_type,true);
		$criteria->compare('source_name',$this->source_name,true);
		$criteria->compare('source_url',$this->source_url,true);
		$criteria->compare('avatar_url',$this->avatar_url,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('published_at',$this->published_at,true);
		$criteria->compare('id',$this->id,true);
		$criteria->compare('tone',$this->tone);
		$criteria->compare('tone_score',$this->tone_score);
		$criteria->compare('alert_id',$this->alert_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Post the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

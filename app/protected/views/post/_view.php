<?php
/* @var $this PostController */
/* @var $data Post */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('uid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->uid), array('view', 'id'=>$data->uid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('post_type_id')); ?>:</b>
	<?php echo CHtml::encode($data->post_type_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('source_name')); ?>:</b>
	<?php echo CHtml::encode($data->source_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('source_url')); ?>:</b>
	<?php echo CHtml::encode($data->source_url); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('avatar_url')); ?>:</b>
	<?php echo CHtml::encode($data->avatar_url); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('url')); ?>:</b>
	<?php echo CHtml::encode($data->url); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('published_at')); ?>:</b>
	<?php echo CHtml::encode($data->published_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::encode($data->id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tone')); ?>:</b>
	<?php echo CHtml::encode($data->tone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tone_score')); ?>:</b>
	<?php echo CHtml::encode($data->tone_score); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alert_id')); ?>:</b>
	<?php echo CHtml::encode($data->alert_id); ?>
	<br />

	*/ ?>

</div>
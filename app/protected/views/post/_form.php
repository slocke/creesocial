<?php
/* @var $this PostController */
/* @var $model Post */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'post-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'post_type_id'); ?>
		<?php echo $form->textField($model,'post_type_id',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'post_type_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'source_name'); ?>
		<?php echo $form->textField($model,'source_name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'source_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'source_url'); ?>
		<?php echo $form->textField($model,'source_url',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'source_url'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'avatar_url'); ?>
		<?php echo $form->textField($model,'avatar_url',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'avatar_url'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'url'); ?>
		<?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'url'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'published_at'); ?>
		<?php echo $form->textField($model,'published_at',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'published_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tone'); ?>
		<?php echo $form->textField($model,'tone',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'tone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tone_score'); ?>
		<?php echo $form->textField($model,'tone_score'); ?>
		<?php echo $form->error($model,'tone_score'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alert_id'); ?>
		<?php echo $form->textField($model,'alert_id',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'alert_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
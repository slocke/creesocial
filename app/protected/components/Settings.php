<?php 

class Settings extends CComponent
{
	public function init() {
		
	}
	
	public function save($key, $value) {
			
		$oldSettings = ConfigSetting::model()->findAllByAttributes(array('name' => $key));
		
		foreach ($oldSettings as $thisSetting) {
			$thisSetting->delete();
		}
		
		$newSetting = new ConfigSetting;
		$newSetting->name = $key;
		$newSetting->value = $value;
		return $newSetting->save();
	}
	
	public function get($key) {
		$setting = ConfigSetting::model()->findByAttributes(array('name' => $key));
		if ($setting) {
			return $setting->value;
		} else {
			return false;
		}
	}

}
<?php 

class Mention extends CComponent
{
	
	public $access_token;
	public $curlOpts;
	public $account_id;
	
	public function init() {
		$this->access_token = Yii::app()->params['mention_access_token'];
		$this->curlOpts = array(
			'Authorization' => 'Bearer '.$this->access_token,
			'Accept' => 'application/json'
		);
		$this->account_id = $this->getAccountId();
	}
	
	
	public function getAccountId() {
		$url = "https://api.mention.net/api/accounts/me";
		$response = Yii::app()->curl->setHeaders($this->curlOpts)->get($url);
		
		$data = json_decode($response);
		$path = $data->_links->me->href;
		
		// fetching account
		
		$url = "https://api.mention.net".$path;
		
		
		$response = Yii::app()->curl->setHeaders($this->curlOpts)->get($url);
		
		
		
		
		
		$data = json_decode($response);
		$account = $data->account;
		$account->id;
		
		return $account->id;
	}

	public function getAlerts() {
		

		$url = "https://api.mention.net/api/accounts/".$this->account_id."/alerts";

		$response = Yii::app()->curl->setHeaders($this->curlOpts)->get($url);
		
		$output = json_decode($response);

		return $output->alerts;	
	}
	
	public function saveMentions($alert_id) {
		
		$criteria = new CDbCriteria;
		$criteria->order = "id DESC";
		$criteria->compare('alert_id', $alert_id);
		
		$lastMention = Post::model()->find($criteria);
		var_dump($lastMention);
		if ($lastMention) {
			$params = array('limit' => 100, 'since_id' => $lastMention->id);
		} else {
			$params = array('limit' => 100);
		}

		$url = "https://api.mention.net/api/accounts/".$this->account_id."/alerts/".$alert_id."/mentions";

		$response = Yii::app()->curl->setHeaders($this->curlOpts)->get($url, $params);

		$output = json_decode($response);
		$mentions = $output->mentions;
		
		foreach ($mentions as $thisMention) {
			$mention = new Post;
			$mention->attributes = (array)$thisMention;	
			$mention->save();
			var_dump($mention->id);
		}
	
		return $mentions;	
	}
	

}
<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}
	
	
	public function actionTest() {
		foreach (Yii::app()->mention->getAlerts() as $thisAlert) {
			
			Yii::app()->mention->saveMentions($thisAlert->id);
			
				
		}
		
	}
	
	public function actionPullFacebook() {
		$response = Yii::app()->facebook->api('/'.Yii::app()->params['facebook_page_id'].'/feed', 'GET');
		$since = $this->getBetween($response['paging']['previous'], 'since=', '&');
		Yii::app()->settings->save('fb_since', $since);
		
		
	}

	public function getBetween($content,$start,$end){
	    $r = explode($start, $content);
	    if (isset($r[1])){
	        $r = explode($end, $r[1]);
	        return $r[0];
	    }
	    return '';
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()

	{


		echo json_encode(Post::model()->limit(5)->findAll());
		

		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	public function getValue($field)
	{
        $fields = explode(":", $field);
        $fields = explode("\"", $fields[1]);
        return $fields[1];
	}

	public function actionFbOauth() {
		
		$appId = Yii::app()->params['facebook_app_id'];
		$secret = Yii::app()->params['facebook_app_secret'];
		$page_id = Yii::app()->params['facebook_page_id'];
		$pageUri=urlencode("http://dev.localhost/site/fbOauth");
		if(!isset($_GET["code"]))
		{
		        $codeUri="https://graph.facebook.com/oauth/authorize?client_id=$appId"
		                ."&redirect_uri=$pageUri&scope=offline_access,manage_pages";
		        header("Location: $codeUri");
		}
		else
		{
		        $oauthUri="https://graph.facebook.com/oauth/access_token?client_id="
		                ."$appId&redirect_uri=$pageUri&client_secret=$secret&code="
		                .urlencode($_GET["code"])."";
		        $contents = @file_get_contents($oauthUri);
		        echo "<h2>Your oauth key</h2>";
		        $userData = array();
		        foreach(explode("&", $contents) as $variable)
		        {
		                $array = explode("=", $variable);
		                $userData[$array[0]] = $array[1];
		                echo "<b>{$array[0]}</b>: {$array[1]}</br>";
		        }
		        $oauthUri="https://graph.facebook.com/me/accounts?access_token="
		                .urlencode($userData["access_token"]);
		        $contents = @file_get_contents($oauthUri);
				
				$decoded_contents = json_decode($contents);
				
				foreach ($decoded_contents->data as $thisPage) {
					if ($thisPage->id == $page_id) {
						$pageName = $thisPage->name;
						$pageToken = $thisPage->access_token;
						$oldSettings = ConfigSetting::model()->findAllByAttributes(array('key' => 'facebook_page_token'));
						foreach ($oldSettings as $thisSetting) {
							$thisSetting->delete();
						}
						$newSetting = new ConfigSetting;
						$newSetting->key = 'facebook_page_token';
						$newSetting->value = $pageToken;
						$newSetting->save();
					}
				}
				
		    
		        echo "<h2>Your pages oauth key</h2>";
		     
		                echo "<b>{$pageName}</b>: {$pageToken}</br>";
		      
		}
		
		
	}
	
}